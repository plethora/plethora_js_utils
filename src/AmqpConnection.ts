import { connect as connectAmqp, Options, Channel, Replies, Message, Connection } from 'amqplib';
// tslint:disable-next-line:import-name
import * as Observable from 'zen-observable';
import * as Promise from 'bluebird';

export default class AmqpConnection {
    private connectOptions: Options.Connect;
    private connection: Connection;
    private channel: Channel;

    constructor(amqpConnectionOptions: Options.Connect) {
        this.connectOptions = amqpConnectionOptions;
    }

    connect(): Promise<Connection> {
        if (this.connection) return Promise.resolve(this.connection);
        return connectAmqp(this.connectOptions)
            .then((connection) => {
                this.connection = connection;
                return connection;
            });
    }

    createChannel(): Promise<Channel> {
        if (this.channel) return Promise.resolve(this.channel);
        return this.connect()
            .then((connection: Connection) => connection.createChannel())
            .then((channel) => {
                this.channel = channel;
                return channel;
            });
    }

    /**
     * This method connects to the Plethora RabbitMq server using the AMQP protocol.
     * it Assert the Exchange (topic) and the Queue, and then consume messages from
     * the queue. It returns an observable of the messaages.
     *
     * ```
     * const connectionOptions = {
     *     protocol: 'amqp',
     *     hostname: process.env.AMQP_HOSTNAME,
     *     port: Number(process.env.AMQP_PORT),
     *     username: process.env.AMQP_USER,
     *     password: process.env.AMQP_PASS,
     *     vhost: process.env.AMQP_VHOST,
     * };
     *
     * retrieveTopicMessage(
     *     connectionOptions,
     *     'connection_exchange',
     *     'exchange_connection',
     * )
     * .then((msgObservable: Observable<Message>) => {
     *      msgObservable
     *          .subscribe((msg: Msg) => {
     *              console.log(msg.content);
     *          })
     * })
     * .catch((err) => { console.error(err); });
     * ```
     *
     * @param connectOptions
     * @param exchange
     * @return {Promise<Message>}
     */
    retrieveTopicMessage(
        exchange: string,
        topic: string | string[],
    ): Promise<Observable<Message>> {
        return this.createChannel()
            .then(channel => Promise.all([
                channel,
                channel.assertExchange(exchange, 'topic', { durable: false }),
            ]))
            .then(([channel]) =>
                Promise.all([
                    channel,
                    channel.assertQueue('', { exclusive: true }),
                ]),
            )
            .then(([channel, q]) => {
                Array.isArray(topic)
                    ? topic.forEach(t => channel.bindQueue(q.queue, exchange, t))
                    : channel.bindQueue(q.queue, exchange, topic);
                return new Observable<Message>((observer) => {
                    channel.consume(q.queue, (msg: Message) => {
                        if (!msg) return;
                        observer.next(msg);
                    });
                });
            });
    }

    /**
     * This method connects to a queue and retrieve every message from it
     *
     * ```
     * retrieveQueueMessage(connectOpt)
     * .then((msgObservable) => {
     *      msgObservable((msg: Message) => {
     *          console.log(msg.content);
     *      });
     * })
     * .catch((err) => { console.error(err); });
     * ```
     * @return {Promise<Observable<Message>>}
     */
    retrieveQueueMessage(
        queue: string,
    ) {
        return this.createChannel()
            .then((channel: Channel) => Promise.all([
                channel,
                channel.assertQueue(queue),
            ]))
            .then(([channel]) => {
                return new Observable<Message>((observer) => {
                    channel.consume(queue, (msg: Message | null) => {
                        if (!msg) return;
                        observer.next(msg);
                    });
                });
            });
    }

    /**
     * it publishes a message to an exchange topic, it then resolves
     * @param exchange
     * @param topic
     * @param msg
     */
    publishOnTopic(
        exchange: string,
        topic: string,
        msg: Buffer,
    ): Promise<void> {
        return this.createChannel()
            .then((channel: Channel) => Promise.all([
                channel,
                channel.assertExchange(exchange, 'topic', { durable: false }),
            ]))
            .then(([channel]) => {
                const done = channel.publish(
                    exchange,
                    topic,
                    msg,
                );
                return done
                    ? Promise.resolve()
                    : Promise.reject(`Failed to send ${msg} to ${topic}`);
            });
    }
}
