import { MongoClient, Db } from 'mongodb';

import getPercentageThreshold from '../dist/getPercentageThreshold'

const getRandom = max => min => () =>
    Math.floor(
        Math.random() * (max - min) + min,
    );

const between50And100 = getRandom(100)(50);
const timestamp = new Date().getTime();
MongoClient.connect('mongodb://localhost:27017/PLETHORA_TEST')
    .then((db: Db) => {
        const values = Array.apply(null, Array(40))
            .map((_, i) => ({
                device_id: 'DEVICE_ID',
                module_id: 'MODULE_ID',
                timestamp: timestamp - i,
                data: {
                    key: 'RAM_LOAD',
                    value: between50And100(),
                }
            }));
        const avg = values
            .sort((a, b) => a.timestamp - b.timestamp)
            .slice(0, 30)
            .map(object => object.data.value)
            .reduce((sum, v) => sum + v, 0) / 30 * 1.1;
        db.collection('messages')
            .insertMany(values)
            .then(() => getPercentageThreshold(db, 'DEVICE_ID', 'MODULE_ID'))
            .then((threshold) => {
                if (threshold === avg) {
                    console.log('OK');
                } else {
                    console.log(threshold, avg, 'NOK');
                }

                return db.collection('messages')
                    .drop();
            })
            .then(() => {
                console.log('DONE!')
                db.close();
            });
    })
    .catch(err => console.error(err));
