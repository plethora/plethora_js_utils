/// <reference types="bluebird" />
/// <reference types="zen-observable" />
/// <reference types="node" />
import { Options, Channel, Message, Connection } from 'amqplib';
import * as Observable from 'zen-observable';
import * as Promise from 'bluebird';
export default class AmqpConnection {
    private connectOptions;
    private connection;
    private channel;
    constructor(amqpConnectionOptions: Options.Connect);
    connect(): Promise<Connection>;
    createChannel(): Promise<Channel>;
    retrieveTopicMessage(exchange: string, topic: string | string[]): Promise<Observable<Message>>;
    retrieveQueueMessage(queue: string): Promise<Observable<Message>>;
    publishOnTopic(exchange: string, topic: string, msg: Buffer): Promise<void>;
}
