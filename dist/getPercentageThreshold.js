"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getPercentageThreshold(db, deviceId, moduleId) {
    const limit = 30;
    return db.collection('messages').find({ module_id: moduleId, device_id: deviceId }, { fields: { _id: 0 } })
        .sort({ timestamp: -1 })
        .limit(limit)
        .toArray()
        .then(data => data.length >= limit
        ? data.reduce((sum, row) => sum + (Number(row.data.value) || 0), 0) / limit * 1.1
        : 100);
}
exports.default = getPercentageThreshold;
