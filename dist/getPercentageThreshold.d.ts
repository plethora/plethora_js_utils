import { Db } from 'mongodb';
export default function getPercentageThreshold(db: Db, deviceId: string, moduleId: string): Promise<number>;
