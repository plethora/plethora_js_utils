"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const amqplib_1 = require("amqplib");
const Observable = require("zen-observable");
const Promise = require("bluebird");
class AmqpConnection {
    constructor(amqpConnectionOptions) {
        this.connectOptions = amqpConnectionOptions;
    }
    connect() {
        if (this.connection)
            return Promise.resolve(this.connection);
        return amqplib_1.connect(this.connectOptions)
            .then((connection) => {
            this.connection = connection;
            return connection;
        });
    }
    createChannel() {
        if (this.channel)
            return Promise.resolve(this.channel);
        return this.connect()
            .then((connection) => connection.createChannel())
            .then((channel) => {
            this.channel = channel;
            return channel;
        });
    }
    retrieveTopicMessage(exchange, topic) {
        return this.createChannel()
            .then(channel => Promise.all([
            channel,
            channel.assertExchange(exchange, 'topic', { durable: false }),
        ]))
            .then(([channel]) => Promise.all([
            channel,
            channel.assertQueue('', { exclusive: true }),
        ]))
            .then(([channel, q]) => {
            Array.isArray(topic)
                ? topic.forEach(t => channel.bindQueue(q.queue, exchange, t))
                : channel.bindQueue(q.queue, exchange, topic);
            return new Observable((observer) => {
                channel.consume(q.queue, (msg) => {
                    if (!msg)
                        return;
                    observer.next(msg);
                });
            });
        });
    }
    retrieveQueueMessage(queue) {
        return this.createChannel()
            .then((channel) => Promise.all([
            channel,
            channel.assertQueue(queue),
        ]))
            .then(([channel]) => {
            return new Observable((observer) => {
                channel.consume(queue, (msg) => {
                    if (!msg)
                        return;
                    observer.next(msg);
                });
            });
        });
    }
    publishOnTopic(exchange, topic, msg) {
        return this.createChannel()
            .then((channel) => Promise.all([
            channel,
            channel.assertExchange(exchange, 'topic', { durable: false }),
        ]))
            .then(([channel]) => {
            const done = channel.publish(exchange, topic, msg);
            return done
                ? Promise.resolve()
                : Promise.reject(`Failed to send ${msg} to ${topic}`);
        });
    }
}
exports.default = AmqpConnection;
